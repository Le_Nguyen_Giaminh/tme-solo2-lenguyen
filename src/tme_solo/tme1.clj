
;;;; ================================================
;;;; = 3I020 - Principes des programmes déclaratifs =
;;;; ================================================
;;;; = TME Solo - édition 2018/2019                 =
;;;; ================================================

(ns tme-solo.tme1
  (:use midje.sweet))

;;; DUREE : 45 minutes

;;; IMPORTANT
;;; - les trois exercices du TME sont indépendants
;;; - pendant le TME il est interdit de communiquer, par quelque moyen que ce soit
;;;   avec d'autres étudiants
;;; - tous les supports de cours sont autorisés, de même que l'accès aux documents
;;;   clojure en ligne.

;;; Exercice 1 :
;;; ==========

;;; Question 1.1:

;;; Compléter la définition de fonction suivante

;; (into [] (rseq [1 2 3 4 5]))

(defn reverse-vect [v]
  (let [r (reverse v)]
    (vec r)))

;; afin de valider les tests ci-dessous:

(fact "à propose de `reverse-vect`."
       (reverse-vect []) => []

       (reverse-vect [2]) => [2]

       (reverse-vect [:a :b :c :d]) => [:d :c :b :a])

;;; Question 1.2:

;;; Compléter la définition de fonction suivante :

(defn map-reduce
  "Map avec la fonction `f`
  et reduit avec `g` et `init` la séquence `s`."
  [f g init s]
  (loop [s s, res init]
    (if (seq s)
      (recur (rest s) (g res (f (first s))))
      res)))

;; afin de valider les tests ci-dessous:

(fact "à propos de `map-reduce`."
      (map-reduce #(* % %) + 0 (range 1 5)) => 30  ;; = 1^² + 2^² + 3² + 4²

      (map-reduce (fn [a] (+ 1 a)) + 0 (range 1 7)) => 27

      (map-reduce (fn [a] (* 4 a)) - 4 [1 2 3]) => -20

      (map-reduce (fn [a] (* 4 a))
                  (fn [a b]
                    (if (> a b)
                      a
                      b)) 4
                  [1 2 3]) => 12)

;; Important : on obtient l'intégralité des points à la question si
;; on n'effectue qu'un seul passage dans la séquence en entrée.

;;; Exercice 2 :
;;; ==========

;;; Compléter la définition de fonction suivante :
;;; Dans cet exercice, l'efficacité de votre programme est un critère important

;;; Question 2.1:
;;;  Compléter la définition de fonction suivante :

(defn sum-seq
  "Retourne la séquence des nombres cummulés."
  ([] (lazy-seq (cons 0 (sum-seq 1))))
  ([n] (lazy-seq (cons (/ (+ (* n n) n) 2) (sum-seq (inc n))))))

(fact "à propos de `sum-seq"
     (take 11 (sum-seq)) => '(0 1 3 6 10 15 21 28 36 45 55)

     (first (drop 100 (sum-seq))) => (quot (* 100 101) 2))

;;; Question 2.2:
;;; Compléter la définition de fonction suivante :

(defn map-filter-lazy
  [f pred? s]
  (if (seq s)
    (lazy-seq (if (pred? (f (first s)))
                (cons (f (first s)) (map-filter-lazy f pred? (rest s)))
                (map-filter-lazy f pred? (rest s))))
    ()))

(fact "à propos de `map-filter-lazy`."
  (take 10 (map-filter-lazy (fn [a] a) (constantly true) (range)))
  => '(0 1 2 3 4 5 6 7 8 9)

  (take 10 (map-filter-lazy (fn [a] a) odd? (range)))
  => '(1 3 5 7 9 11 13 15 17 19)

  (first (drop 1000000 (map-filter-lazy (fn [a] (* 3 a)) even? (range))))
  => 6000000


  ;; Attention : stack overflow potentiel ici...
  (take 4 (map-filter-lazy #(rem % 200000) zero? (range))) => '(0 0 0 0)

)

;;; Exercice 3
;;; ==========

;;; La macro `if-let` de Clojure permet
;;; de simplifier la gestion des expressions retournant nil.

(fact "A propos de if-let"

      ;; Exemple 1 : cas positif
      (if-let [[a b] (get {:x [4 2], :y [3 5]} :x)]
        (+ a b)
        ;; cas nil
        :failed)  => 6

      (let [res (get {:x [4 2], :y [3 5]} :x)]
        (if res
          (let [[a b] res]
            (+ a b))
          :failed)) => 6

      ;; Exemple 2 : cas négatif
      (if-let [[a b] (get {:x [4 2], :y [3 5]} :z)]
        (+ a b)
        ;; cas nil
        :failed) => :failed)


;;; Redéfinir cette macro sous le nom `my-if-let` sans
;;; utiliser la définition originale de `if-let` bien sûr.


(defmacro my-if-let [[binding cond] then-expr nil-expr]
  nil)

(facts "a propos de my-if-let"
      ;; Exemple 1 : cas positif
      (my-if-let [[a b] (get {:x [4 2], :y [3 5]} :x)]
        (+ a b)
        ;; cas nil
        :failed)  => 6

      ;; Exemple 2 : cas négatif
      (my-if-let [[a b] (get {:x [4 2], :y [3 5]} :z)]
        (+ a b)
        ;; cas nil
        :failed) => :failed)
